DESCRIPTION OF THE UPDATE to v1.1.0:

* pyPipe3D updates:

** INDICES: bugfix central spaxel value (segmentation loop missing the central spaxel index)
** logging procedure (SEE pyPipe3D README)
** pyFIT3D now will not fit the EL (fit_gas=False) at the first central spectrum (5”) analysis. (In the future this entire first run of pyFIT3D analysis for the central spectrum should be replaced by a single redshift and sigma calculation using pyFIT3D routines.)
** Add a badpixel mask (3 dimensions λ, y, x) support. (SEE pyPipe3D README)
** Bugfix when manga is present in the name of the cube and the name does not follow the manga-plate-ifu pattern.
** ReadDatacubes help update.
** ReadDatacubes bugfix when a cube is not available.
** ReadDatacubes now will read the flux_elines_long.NAME.cube.fits.gz file also when available.

* pyFIT3D updates:

** bugfix print blank lines when using guide_{vel,sigma}().
** fit_elines run_modes (RND, LM, BOTH) names case insensitive (inside the function the names are all uppercase).
** bugfix when the automatic wavelength range (correct_wl_range=2) is set into the fit_elines execution in StPopSynt._EL_fit().
** bugfix poly1d coeffs print in EmissionLinesLM.output_to_screen().

* tools updates:

** indices_spec() created. One spectrum indices analysis.
** radial_sum_cube_e() now also reads a badpixel mask.
