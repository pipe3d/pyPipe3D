;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; pyFIT3D single data cube analysis configuration file
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
; NOTES TO THE CONFIGURATION FILE
; -------------------------------
; Welcome to the ana_single.py example configuration file.
;
; * Please read also the READ ME file "examples/IFS_example/README.txt"
;
; * This file works as an INI file. Variables can be set as:
;   KEY = VALUE
;
; * Commented attributions will be get as None
;
; * if a KEY is defined as blank it will be understood depending on the get
;   function used by the ConfigParser() adopted on the script.
;
; * Sections are defined as:
;   [SECTION]
;
; * Values from same section can be used as variable using the key as ${KEY}.
;   If the KEY is in a different section also can be retrieved as ${SECTION:KEY}.
;
;   Example:
;   --------
;   [FOO]
;   A = 1
;   B = ${A}      ; attributes the same value as the key A
;   [BOO]
;   a = ${FOO:A}  ; attributes the same value as the key A in
;                 ; section FOO
;
; * Inside the configuration file, the word NAME along the filenames will be
;   replaced by the actual name of the object passed by the user during the
;   ana_single.py execution.
;
; * In file commentaries could be preceded by a semicolon (;) or a hashtag (#).
;
; * yes and no are the boolean equivalents for True and False.
;
; * The wildcard NAME will be replaced by the galaxy name inputed by the user
;   when calling "ana_single.py" script.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; General section
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
[general]
; Stops ana_single.py run before proceeding to the first RSS pyFIT3D analysis.
; It will generate all output files to this part.
debug = no
; Verbose for all functions that accept verbosity.
verbose = no
; Save all files from all scripts (including temporary files). If set to no it
; can save some time in writing to the disk.
save_aux_files = yes
; Set the compression level used for the gzip python module.
gzip_compresslevel = 6

; PLOT (0/1/2)
; With the plot active, the analysis time will increase.
; 0 - do not plot
; 1 - plot to screen
; 2 - plot to file
plot = 0

; Uncomment the next line if you want to set the seed to the random numbers
; generator (under construction, not fully tested).
;seed = 123456789

; dir_data is the directory where the cube is stored
dir_data = data

; The path to the cube filename. It should include the NAME wildcard in order to
; be replaced during the script execution (see the NOTES TO THE CONFIGURATION
; FILE above; see also dir_data description).
cube_filename = ${dir_data}/NAME.V500.rscube.fits.gz

; Directory to output the analysis
dir_out = out
; Directory where the data cubes will be copied.
dir_datacubes_out = out

; Directory where the ancillary configuration files are stored.
dir_conf = config

; The path to the pseudo V-band slice configuration file.
slice_config_file = ${dir_conf}/slice_V.conf

; Log filenames
; NOTE:
;   * If a file is not provide, a log is not created.
;   * The standard outputs /dev/* (e.g. /dev/stdout) are accepted.
;   * if the string NAME is in the filename, a log will be created with the
;     galaxy filename provided by the user at the run and the log file open_mode
;     will be "w". If not, the open_mode will be "a".
log_file = ana_single.NAME.log
;log_file = /dev/stdout
; Only used by ana_single_kin.py
kin_log_file = ana_single_kin.NAME.log

; Force spatial mask fits file (2D; Y,X).
;spatial_mask_file = ${dir_conf}/NAME.spatialmask.fits.gz

; Mask stars (a.k.a. spaxels brighter than the center) in FoV.
; If “yes” it will look for spaxels brighter than the center and create a
; spatial mask. A “no” will turn off the mask.
mask_stars = no

; Badpix mask:
; NOTE:
;   * The bad pixels mask will be collapsed onto a 2D spatial mask. Bad pixels
;     are identified by the badpix_value. If a bad pixels cube mask is available,
;     a bad spaxels mask will be created for spaxels with a fraction of bad
;     pixels equal or above badpix_coll_threshold_fraction. The output masked
;     spaxels map will identify masked spaxels by the badpix_outmask_value.
badpix_coll_threshold_fraction = 0.75
badpix_outmask_value = 1
; MaNGA
;badpix_extension = 2
;badpix_value = 1027
; CALIFA
badpix_extension = 3
badpix_value = 1

; Instrumental dispersion (in Angstroms)
; NOTE:
;	  * This is the quadratic difference between the instrumental dispersion of
; 	  the data and the SSP models.
instrumental_dispersion = 2.6

; Dust model configuration
; Extinction law:
;   * CCM : Cardelli, Clayton & Mathis (1989)
;   * CAL : Calzetti et al. (1994)
dust_extlaw = CCM
dust_RV = 3.1

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; SSP templates section
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
[ssp_models]
; USE: template_nickname = path/to/template.fits
gsd01_156 = ssp/gsd01_156.fits
gsd01_12 = ssp/gsd01_12.fits

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Auto SSP configuration files section
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
[auto_ssp_config]
; USE: config_nickname = path/to/auto_ssp_file.config
; NOTE:
;   * "no_lines" should be deprecated and replaced by the option gas_fit=False
;     in pyFIT3D routines.
no_lines = ${general:dir_conf}/auto_ssp_V500_no_lines.config
eml_systems_kin_cube = ${general:dir_conf}/auto_ssp_V500_several_SII_z.config
; NOTE:
;   * "strong" config_nickname is mandatory
strong = ${general:dir_conf}/auto_ssp_V500_several.config

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; General single spectrum analysis (central and integrated spectrum)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Section to configure the initial analysis with central and
; integrated spectra.
[general_single_analysis]
; central spectrum coordinates
; NOTE:
;   * The central coordinates could be forced when calling ana_single.py.
;     (SEE "ana_single.py" USAGE)
x0 = 36.37
y0 = 31.96

; Central and integrated spectrum
; Radial cumulative sum apperture (in spaxels)
; NOTE:
;   * It defines the central (2*radial_sum_cube_delta_R) and integrated
;     (10*radial_sum_cube_delta_R) spectra area of extraction.
radial_sum_cube_delta_R = 2.5
; Output spectra filenames
cen_spec_filename = NAME.spec_5.txt
int_spec_filename = NAME.spec_30.txt

; deprecated, see: ${cube_segmentation:pseudo_V_band_file}
pseudo_V_band_file = NAME.V.fits

; Redshift determination.
; -----------------------
; The range and delta values should be in km/s, i.e. they will be
; divided by the light velocity.
;
;;;;;;; disabled block ;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; auto define redshift range using vel_eline()
;vel_eline_reference_wavelength = 3728
;vel_eline_min_wavelength = 3800
;vel_eline_max_wavelength = 4000
;vel_eline_nsearch = 2
;vel_eline_imin = 0.7
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
; ---- Force redshift ----
; Force search of redshift around redshift. It also can be set in execution
; time (SEE "ana_single.py" USAGE).
force_redshift = 0.012225
; km/s
redshift_half_range = 600
; NOTE:
;    If force_redshift is set (by this configuration file or by the user in
;    execution time) the script will use this value as input redshift, and the
;    redshift half range will configure the redshift range to:
;
;       redshift_min_max = force_redshift +/- (redshift_half_range/c)
;
;    and the step (delta) of the redshift search to:
;
;       delta_redshift = 0.2 * (redshift_half_range/c)
;
; ---- Redshift search range ----
; If the redshift is not forced, the pipeline will set the input, max/min and
; delta to the below values. Avoid this configuration because
input_redshift = 0.2
min_redshift = -0.001
max_redshift = 0.2
delta_redshift_kms = 300

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; pyFIT3D (auto_ssp) config for the central and integrated spectra analysis
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; The following sections harbour the pyFIT3D input configuration variables for
; the central and integrated spectra analysis.

; Central spectrum analysis with no emission lines fit.
[cen_auto_ssp_no_lines]
output_filename = auto_ssp_Z.NAME.cen.out
min_wavelength = 3800
max_wavelength = 7000
models_file = ${ssp_models:gsd01_156}
nonlinear_min_wavelength = 3850
nonlinear_max_wavelength = 4700
nonlinear_models_file = ${ssp_models:gsd01_12}
config_file = ${auto_ssp_config:no_lines}
mask_file = ${general:dir_conf}/mask_elines.txt
emission_lines_mask_file = ${general:dir_conf}/emission_lines.txt
input_AV = 0
delta_AV = 0.15
min_AV = 0
max_AV = 1.6
; values in Angstrom
input_sigma = 1.6
delta_sigma = 0.5
min_sigma = 1.2
max_sigma = 5.5
RV = ${general:dust_RV}
extlaw = ${general:dust_extlaw}
fit_sigma_rnd = yes
; S/N calculation window
; NOTE:
;    Wavelength at observed-frame.
;    If SN_window_reference_wavelength is commented it
;    will use SSP Models normalization wavelength.
;SN_window_reference_wavelength = 5500
SN_window_half_range = 45
; Normalization window
; NOTE:
;    Wavelength at rest-frame
;    If SN_window_reference_wavelength is commented it
;    will use SSP Models normalization wavelength.
;norm_window_reference_wavelength = 5500
norm_window_half_range = 45

; Central spectrum analysis.
[cen_auto_ssp_inst_disp]
; Wavelengths range, redshift inputs and config file are
; defined by the cen_auto_ssp_no_lines analysis.
no_mask_output_filename = auto_ssp_no_mask.NAME.cen.out
output_filename = auto_ssp.NAME.cen.out
; Angstroms
instrumental_dispersion = ${general:instrumental_dispersion}
;min_wavelength = 3800
;max_wavelength = 7000
models_file = ${ssp_models:gsd01_156}
;nonlinear_min_wavelength = 3850
;nonlinear_max_wavelength = 4700
nonlinear_models_file = ${ssp_models:gsd01_12}
; config_file = ${auto_ssp_config:no_lines}
mask_file = ${general:dir_conf}/mask_elines.txt
emission_lines_mask_file = ${general:dir_conf}/emission_lines.txt
input_AV = 0
delta_AV = 0.15
min_AV = 0
max_AV = 1.6
; values in km/s
input_sigma = 30
delta_sigma = 20
min_sigma = 1
max_sigma = 400
RV = ${general:dust_RV}
extlaw = ${general:dust_extlaw}
fit_sigma_rnd = yes

; Integrated spectrum analysis.
[int_auto_ssp_inst_disp]
; Wavelengths range, redshift inputs and config file are
; defined by the cen_auto_ssp_no_lines analysis.
; Sigma input and max value are defined by cen_auto_ssp_inst_disp analysis.
output_filename = auto_ssp.NAME.int.out
; Angstroms
instrumental_dispersion = ${general:instrumental_dispersion}
;min_wavelength = 3800
;max_wavelength = 7000
models_file = ${ssp_models:gsd01_156}
;nonlinear_min_wavelength = 3850
;nonlinear_max_wavelength = 4700
nonlinear_models_file = ${ssp_models:gsd01_12}
;config_file = ${auto_ssp_config:no_lines}
mask_file = ${general:dir_conf}/mask_elines.txt
emission_lines_mask_file = ${general:dir_conf}/emission_lines.txt
input_AV = 0
delta_AV = 0.15
min_AV = 0
max_AV = 1.6
; values in km/s
;input_sigma = 30
delta_sigma = 20
min_sigma = 1
;max_sigma = 400
RV = ${general:dust_RV}
extlaw = ${general:dust_extlaw}
fit_sigma_rnd = yes

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Cube segmentation (CS-binning; RSS creation)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
[cube_segmentation]
; CS-binning options
flux_limit_peak = 0.02
target_SN = 50
min_SN = 1
frac_peak = 0.9
min_flux = 0.001

; Creation of the maps from the pyFIT3D RSS analysis
; NOTE:
;   Both following values are used to correct the calculated stellar velocity
;   dispersion in km/s:
;
;   disp [km/s] = (c/wave_norm)*sqrt(abs(disp**2 - instrumental_dispersion**2))
instrumental_dispersion = 2
wave_norm = 5500

; output filenames
pseudo_V_band_file = NAME.V.fits
cont_seg_file = cont_seg.NAME.fits
scale_seg_file = scale.seg.NAME.fits
ssp_mod_file = SSP_mod.NAME.cube.fits
gas_cube_file = GAS.NAME.cube.fits
gas_seg_file = GAS.CS.NAME.RSS.fits
map_output_prefix = map.CS.NAME

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; pyFIT3D (auto_ssp) config for the first RSS analysis
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
[auto_ssp_rss_sigma_guided]
; i.e. disp_min
; Wavelengths range, redshift inputs and config file are
; defined by the cen_auto_ssp_no_lines analysis.
; Sigma input and max value are defined by cen_auto_ssp_inst_disp analysis.
output_filename = auto_ssp.CS_few.NAME.rss.out
; Angstroms
instrumental_dispersion = ${general:instrumental_dispersion}
;min_wavelength = 3800
;max_wavelength = 7000
models_file = ${ssp_models:gsd01_156}
;nonlinear_min_wavelength = 3850
;nonlinear_max_wavelength = 4700
nonlinear_models_file = ${ssp_models:gsd01_12}
; config_file = ${auto_ssp_config:no_lines}
mask_file = ${general:dir_conf}/mask_elines.txt
emission_lines_mask_file = ${general:dir_conf}/emission_lines.txt
input_AV = 0
delta_AV = 0.15
min_AV = 0
max_AV = 1.6
; values in Angstrom
;input_sigma = 30
delta_sigma = 20
min_sigma = 1
;max_sigma = 400
RV = ${general:dust_RV}
extlaw = ${general:dust_extlaw}
fit_sigma_rnd = yes

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; pyFIT3D (auto_ssp) config for the second RSS analysis
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
[auto_ssp_rss_guided]
; i.e. non-linear values and respective errors from auto_ssp_rss_sigma_guided
; analysis are forced in this run. Also, it analyzes the subtracted stellar
; population spectra from the auto_ssp_rss_sigma_guided analysis.
; Wavelengths range, redshift inputs and config file are
; defined by the cen_auto_ssp_no_lines analysis.
; Sigma input and max value are defined by cen_auto_ssp_inst_disp analysis.
output_filename = auto_ssp.CS.NAME.rss.out
; Angstroms
instrumental_dispersion = 1.601
;min_wavelength = 3800
;max_wavelength = 7000
models_file = ${ssp_models:gsd01_156}
;nonlinear_min_wavelength = 3850
;nonlinear_max_wavelength = 4700
nonlinear_models_file = ${ssp_models:gsd01_12}
config_file = ${auto_ssp_config:no_lines}
mask_file = ${general:dir_conf}/mask_elines.txt
emission_lines_mask_file = ${general:dir_conf}/emission_lines.txt
input_AV = 0
delta_AV = 0.15
min_AV = 0
max_AV = 1.6
; values in Angstrom
;input_sigma = 30
delta_sigma = 20
min_sigma = 1
;max_sigma = 400
RV = ${general:dust_RV}
extlaw = ${general:dust_extlaw}
fit_sigma_rnd = yes

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; pyFIT3D indices analysis section
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
[indices]
output_file = indices.CS.NAME.rss.out
output_cube_file = indices.CS.NAME.cube.fits
n_MonteCarlo = 5

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; pyFIT3D Emission Lines fit section
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
[emission_lines]
; i.e. kin_cube_elines configuration
; Build Halpha map
vel_eline_prefix = ve.NAME
vel_eline_map_file = ${vel_eline_prefix}.vel_map.fits
vel_eline_mask_map_file = ${vel_eline_prefix}.mask_map.fits
vel_eline_reference_wavelength = 6562.68
; km/s
vel_eline_half_range = 200
vel_eline_nsearch = 1
vel_eline_imin = 0.95
; km/s
clean_Ha_map_vel_half_range = 500
; The emission lines systems inside auto_ssp_config_file will
; build the emission lines analysis loop
; output_file prefix is defined by the auto_ssp_config_file wavelength
; range.
output_file = KIN.GAS.prefix.NAME.out
map_output_prefix = map.prefix.NAME
auto_ssp_config_file = ${auto_ssp_config:eml_systems_kin_cube}
vel_half_range = 300
dispersion_guess = 1.2
dispersion_min = 0.5
n_MonteCarlo = 30
n_loops = 3
scale_ini = 0.15
run_mode = RND
;run_mode = LM
;run_mode = both
log_file = KIN.GAS.prefix.NAME.log

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; pyFIT3D Moment analysis section
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
[flux_elines]
n_MonteCarlo = 10
output_file = flux_elines.NAME.cube.fits.gz
elines_list = ${general:dir_conf}/emission_lines.LIST

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Final pack files section
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
[pack]
SFH_file = ${general:dir_conf}/pack_SFH.csv
SSP_file = ${general:dir_conf}/pack_CS_inst_disp.csv
ELINES_file = ${general:dir_conf}/pack_elines_v1.5.csv
sum_mass_age_out_file_1 = MASS.NAME.csv
sum_mass_age_out_file_2 = Mass.NAME.csv
