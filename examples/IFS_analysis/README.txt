pyPipe3D’s pipeline of IFS data cube analysis
*********************************************


Contents
^^^^^^^^

* pyPipe3D’s pipeline of IFS data cube analysis

  * Presentation

    * Cube and row-stacked spectra (*RSS*) files

    * Format of the SSP library template FITS file

    * Notes on the input data preprocessing

  * Pipeline procedure of analysis

  * Output files

    * Main *data cubes*

    * Other output files

  * Example of the pipeline analysis


Presentation
============

This document presents the sequence of procedures adopted by the
**pyPipe3D** pipeline, specially suited for the analysis of a galaxy
*IFS data cube*. The main products of the analysis comprises the maps
of the stellar populations and the ionized gas properties. The
pipeline is standardized, which requires the input data to be pre-
processed and some configuration files to be edited depending on the
required analysis.


Cube and row-stacked spectra (*RSS*) files
------------------------------------------

We define a *“data cube”* as a 3D FITS file with the 2 first
dimensions related to the spatial distribution of the pixels
*(spaxels; X,Y)*, and a 3rd direction comprising the wavelength
information of the IFU data *(Z)*. The *data cubes*, stored in this
way, comprises data with the same spatial step in the *X* and *Y*
direction (normally in *arcsec*), and the same step in wavelength in
the *Z*-direction (normally Angstroms).

The WCS information is stored in the corresponding header entries:

   CRPIX1  => Pixel of the starting position in RA (or X)
   CRVAL1  => Starting position in RA (or X)
   CDELT1  => Spatial extension of a pixel in the X-direction (in arcsec normally)
   CRPIX2  => Pixel of the starting position in DEC (or Y)
   CRVAL2  => Starting position in DEC (or Y)
   CDELT2  => Spatial extension of a pixel in the Y-direction (in arcsec normally)
   CRPIX3  => Pixel of the starting wavelength
   CRVAL3  => Starting Wavelength in AA
   CDELT3  => Wavelength step in AA per pixel

We defined *“RSS”* file or *“Row Stacked Spectrum”* a 2D FITS file
comprising *NY*-individual spectra of the same length *(NX)*, with a
normalized wavelength step and initial wavelength defined by the
header keywords:

   CRPIX1  => Pixel of the starting wavelength
   CRVAL1  => Starting Wavelength in AA
   CDELT1  => Wavelength step in AA per pixel


Format of the SSP library template FITS file
--------------------------------------------

The template libraries used by **pyFIT3D** are *RSS*, for which in
each row we store a single stellar population. In **pyFIT3D** they are
read using the class SSPModels. The *SSP* spectra should have a common
reference wavelength calibration, with the same step in AA per pixel
and the same staring wavelength defined by the header entries:

   CRPIX1
   CRVAL1
   CDELT1

And for each *SSP*, it is required a *NAME* header keyword, with the
following format:

   NAME0 | spec_ssp_00.0891_z0004.spec
   NAME1 | spec_ssp_00.0891_z019.spec
   NAME2 | spec_ssp_00.0891_z030.spec
   NAME3 | spec_ssp_00.4467_z0004.spec

i.e., "spec_ssp_AGE_zMET.spec", where AGE should be the *SSP* age in
*Gyrs*, and *MET* is the metallicity expressed as the *decimals*, ie.,

   Z/H = 0.0004  should be 0004
   Z/H = 0.02    should be 02

These entries are mandatory to allow the pipeline to understand the
inputs.

The normalization wavelength of the models also could be configured in
the header under the keyword *WAVENORM*. If the key *WAVENORM* does
not exists in the header, the class will sweep all the models looking
for the wavelengths where the flux is closer to 1, calculates the
median of those wavelengths and return it. .


Notes on the input data preprocessing
-------------------------------------

*TODO: MISSING DETAILS*


Pipeline procedure of analysis
==============================

The pipeline script of analysis is ana_single.py. When called, the
script will run and produce all the files at the running directory.
Then, at the end of the complete analysis, the pipeline script will
move everything to the configured directories.

The analysis begins with the read of a single configuration input
file, however, other input files are needed along the procedure (e.g.
*SSP* template libraries, configuration files with the emission lines
systems to be analyzed, output data to be stored on final *data
cubes*, masked wavelengths during the analysis, etc). **pyPipe3D**
repository includes a complete example of a data cube, with all needed
configuration files. This example is described at “Example of the
pipeline analysis” Section.

Finally, the pipeline analysis proceed as follows:

1. Read the data cube (object spectra and FITS header). Error spectra,
   bad pixels and bad spaxels masks are also available.

2. Build the signal, noise and SN maps.

3. Create a slice (2D map) of the data cube (3D) representing the
   *V-Band*.

4. Create the spatial mask of the field-of-view.

5. Extract central and integrated spectrum.

6. Define the redshift range of the analysis.

7. Main **pyFIT3D** analysis of the central and integrated spectra.

8. CS-binning (adopted FoV segmentation; *RSS* creation).

9. First round of **pyFIT3D** analysis of the binned data (row-stacked
   spectra, *RSS*). *NOTE*: This analysis will produce the ionized gas
   modelled spectra and the estimation of the non-linear parameters
   (redshift, velocity dispersion and dust attenuation) of the stellar
   populations.

10. Create the ionized gas data cube and *RSS*.

11. Second round of **pyFIT3D** analysis of the binned data. *NOTE*:
    This analysis is performed over the gas-free spectrum (residual
    from the difference between the observed and the gas model
    spectrum). Both the non-linear and emission lines fit steps are
    not performed during this procedure (values are input from **step
    9** analysis).

12. Stellar absorption indices analysis. *NOTE*: Measurement of
    certain stellar absorption strength indices, such as the Lick
    index system.

13. Create all maps of **pyFIT3D** analysis of the binned data.

14. **pyFIT3D** emission lines analysis of the ionized gas cube
    created at **step 10**.

15. Gzip all fits files (*.fits -> .fits.gz*).

16. Moment analysis of the ionized gas cube created at **step 10**.

17. Create the Mass, Age, Metallicity and *SFH* maps.

18. Create final product *data cubes*.

19. Move all files to output directories.


Output files
============

The main *data cubes* produced by the pipeline are divided in five
FITS files. The information stored in three of them (*SSP*, *SFH* and
*ELINES*) is gather from the maps output from the **steps 3, 4, 8, 9,
11** and **14**. Which maps (output files) are stored in which of them
depends on the pack configuration files. The other two are directly
output by *steps 12* (indices) and *16* (flux_elines). The calculated
indices are hard coded in pyFIT3D, however, in the case of the latter,
a list of emission lines could be configured by the user.


Main *data cubes*
-----------------

These are the five main *data cubes* produced by **pyPipe3D**
pipeline:

1. "NAME.SSP.cube.fits.gz": Data output from **steps 3**, **4**,
   **8**, **9** and **11**.

   * Main parameters derived from the analysis of the stellar
     populations, including the LW and MW ages and metallicities, dust
     attenuation and kinematics of the stellar populations.

2. "NAME.SFH.cube.fits.gz": Data output from **step 11**.

   * Weights of the decomposition of the stellar population for the
     adopted *SSPs* templates library. It can be used to derive the
     spatial resolved star formation and chemical enrichment histories
     of the galaxies and the LW and MW properties included in the
     *SSP* dataproducts.

3. "NAME.ELINES.cube.fits.gz": Data output from **step 14**.

   * Flux intensities for the nine stronger emission lines in the
     optical wavelength range, together with the kinematics properties
     of {\rm H}\alpha, derived based on a *Gaussian* fitting of each
     emission line.

4. "indices.NAME.cube.fits.gz": Data output from **step 12**.

   * Set of stellar absorption indices maps.

5. "flux_elines.NAME.cube.fits.gz": Data output from **step 16**.

   * Maps of the main parameters of a set of more than 50 emission
     lines derived using a weighted moment analysis of the ionized gas
     spectra. This analysis depends on the kinematics of {\rm H}\alpha
     derived at **step 14**.


Other output files
------------------

*TODO: MISSING DETAILS*


Example of the pipeline analysis
================================

We prepare an example run of a galaxy IFS data cube analysis placed at
the sub-directory "examples/IFS_analysis". The input data is the
CALIFA IFS V500 data cube for the galaxy NGC 2916. Below we show the
list of needed ancillary data for the analysis of the cube together
with a brief file description:

   pyFIT3D/examples/IFS_analysis
   ├── ana_single_example.ini                 => input configuration file
   ├── config                                 => ancillary configuration files directory
   │   ├── auto_ssp_V500_no_lines.config      => auto_ssp (pyFIT3D) configuration file (no emission lines to be fitted)
   │   ├── auto_ssp_V500_several.config       => auto_ssp (pyFIT3D) configuration file
   │   ├── auto_ssp_V500_several_SII_z.config => auto_ssp (pyFIT3D) configuration file
   │   ├── cont_V500.config                   => emission lines configuration file without emission lines (only continuum)
   │   ├── emission_lines.LIST                => emission lines list for the moment analysis
   │   ├── emission_lines.txt                 => emission lines to be masked during pyFIT3D analysis
   │   ├── Ha_SII_V500.config                 => [NII]+Ha+[SII] emission lines system configuration file
   │   ├── Ha_V500.config                     => [NII]+Ha emission lines system configuration file
   │   ├── Hd_V500.config                     => Hd emission lines system configuration file
   │   ├── Hg_V500.config                     => Hg emission lines system configuration file
   │   ├── mask_elines.txt                    => wavelengths intervals masked during pyFIT3D analysis
   │   ├── OIII_V500.config                   => [OIII]+Hb emission lines system configuration file
   │   ├── OII_V500.config                    => [OII] emission lines system configuration file
   │   ├── pack_CS_inst_disp.csv              => pack configuration file for SSP data cube.
   │   ├── pack_elines_v1.5.csv               => pack configuration file for ELINES data cube.
   │   ├── pack_SFH.csv                       => pack configuration file for SFH data cube.
   │   ├── SII_V500.config                    => [SII] emission lines system configuration file
   │   └── slice_V.conf                       => V-band slice
   ├── data                                   => input data cube directory
   │   └── NGC2916.V500.rscube.fits.gz        => input data cube FITS file
   ├── README.txt                             => This file.
   └── ssp                                    => input SSP templates for the analysis
       ├── gsd01_12.fits                      => GSD (12 templates - 3 ages, 4 metallicity)
       ├── gsd01_156.fits                     => GSD (156 templates - 39 ages, 4 metallicities)
       └── miles_2_gas.fits                   => MILES (6 templates - 3 ages, 3 metallicities)

The pipeline script usage is:

   $ ana_single.py
   USE: ana_single.py NAME CONFIG_FILE [REDSHIFT] [X0] [Y0]
   CONFIG_FILE is mandatory but defaults to ana_single.ini

The **REDSHIFT** and the central coordinates (**X0** and **Y0**) are
optional input information which helps the pipeline analysis. This
information could also be configured inside the *INI* configuration
file (**CONFIG_FILE**; ana_single_example.ini).

To proceed with the pipeline analysis example, run, inside
"IFS_analysis" sub-directory:

   $ pyPipe3D/examples/IFS_analysis> ana_single.py NGC2916 ana_single_example.ini

The example configuration is entirely prepared to run inside
independently of any other ancillary file.

Both, the **REDSHIFT** and the central coordinates of this object are
already written inside the configuration file. However, a single input
configuration file can be used for an entire set of *data cubes*. This
way, turn to not be practical to write those values directly inside
the configuration file, since they are unique for each object. In
order to run the same example forcing an input **REDSHIFT** of
*0.012225* and the central coordinates as **X0** = *36.37* and **Y0**
= *31.96*, run:

   $ pyPipe3D/examples/IFS_analysis> ana_single.py NGC2916 ana_single_example.ini 0.012225 36.37 31.96

At the end of the example analysis the pipeline will create a
directory called "out" and a subdirectory called "out/NGC2916". All
the output files are moved to directory "out/NGC2916" and the five
final *data cubes* (togheter with a couple of other important
resultant files) are also copied to directory "out". The names of the
output directories and the ancillary configuration directory and files
are all configured through the pipeline *INI* configuration file.
