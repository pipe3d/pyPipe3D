#!/usr/bin/env python3
import sys
from pyFIT3D.common.gas_tools import fit_elines

def main():
    try:
        plot = sys.argv[1]
    except IndexError:
        plot = 0
    w_min, w_max = 6600, 6800
    out_file = 'fit_elines.NII_Ha.NGC5947.out'
    fit_elines(
        'NGC5947.cen.gas.txt', 'notebooks/Ha_NII.config',
        6600, 6800, out_file=out_file, mask_file=None,
        run_mode='BOTH', n_MC=20, n_loops=5, scale_ini=0.15,
        redefine_max=True, plot=plot,
    )

if __name__ == '__main__':
    main()
